-- Add migration script here
-- {
--     contributor_tezos_address: string,
--     referrer_tezos_address: string,
--     contributor_cardano_addresses: string[],
--     contributor_evm_addresses: string[]
-- }

CREATE TABLE IF NOT EXISTS contributors (
    contributor_tezos_address       TEXT PRIMARY KEY UNIQUE NOT NULL,
    referrer_tezos_address          TEXT NOT NULL
);

CREATE TABLE IF NOT EXISTS cardano_addresses (
  contributor_tezos_address TEXT NOT NULL,
  contributor_cardano_address TEXT UNIQUE NOT NULL,
  FOREIGN KEY(contributor_tezos_address) REFERENCES contributors(contributor_tezos_address),
  PRIMARY KEY(contributor_tezos_address, contributor_cardano_address)
);

CREATE TABLE IF NOT EXISTS evm_addresses (
  contributor_tezos_address TEXT NOT NULL,
  contributor_evm_address TEXT UNIQUE NOT NULL,
  FOREIGN KEY(contributor_tezos_address) REFERENCES contributors(contributor_tezos_address),
  PRIMARY KEY(contributor_tezos_address, contributor_evm_address)
);
