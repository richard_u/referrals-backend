// CARDANO
const enableCardano = async function () {
  if (typeof cardano !== "undefined") {
    let isEnabled = await cardano.isEnabled();
    if (!isEnabled) {
      let response = await cardano.enable();
      return response;
    }
    return isEnabled;
  }
};

const getAddress = async function () {
  let address = await cardano.getChangeAddress();
  return address;
};

const signData = async function (address) {
  let msg = document.getElementById("cmsg").value;
  let signed = await cardano.signData(address, hexEncode(msg));
  return signed;
};

const hexEncode = (str) => {
  var result = "";
  for (var i = 0; i < str.length; i++) {
    result += str.charCodeAt(i).toString(16);
  }
  return result;
};

const signPayload = () => {
  if (enableCardano()) {
    getAddress()
      .then((address) =>
        signData(address).then((response) =>
          console.log("SIGNED RESPONSE", response)
        )
      )
      .catch((e) => console.error(e));
  }
};

// ETHEREUM
const initialize = () => {
  let accounts = [];
  //Basic Actions Section
  const onboardButton = document.getElementById("connectButton");
  const signButton = document.getElementById("signButton");

  //Created check function to see if the MetaMask extension is installed
  const isMetaMaskInstalled = () => {
    //Have to check the ethereum binding on the window object to see if it's installed
    const { ethereum } = window;
    return Boolean(ethereum && ethereum.isMetaMask);
  };

  const onClickConnect = async () => {
    try {
      // Will open the MetaMask UI
      // You should disable this button while the request is pending!
      await ethereum.request({ method: "eth_requestAccounts" });
    } catch (error) {
      console.error(error);
    }
  };

  const getAccountAddress = async () => {
    accounts = await ethereum.request({ method: "eth_accounts" });
    document.getElementById("account").innerHTML = accounts[0] || "";
  };

  signButton.onclick = async () => {
    try {
      let msg = document.getElementById("emsg").value;
      let from = accounts[0];
      let msgToHex = `0x${hexEncode(msg)}`;
      console.log(msgToHex);
      const sign = await ethereum.request({
        method: "personal_sign",
        params: [from, msgToHex],
      });
      console.log(sign);
    } catch (e) {
      console.error(e);
    }
  };

  //------Inserted Code------\\
  const MetaMaskClientCheck = async () => {
    //Now we check to see if MetaMask is installed
    if (!isMetaMaskInstalled()) {
      //If it isn't installed we ask the user to click to install it
      onboardButton.innerText = "Click here to install MetaMask!";
    } else {
      //If MetaMask is installed we ask the user to connect to their wallet
      onboardButton.innerText = "Connect";
      //When the button is clicked we call this function to connect the users MetaMask Wallet
      onboardButton.onclick = onClickConnect;
      //The button is now disabled
      onboardButton.disabled = true;
    }
  };

  MetaMaskClientCheck().then(getAccountAddress);
  //------/Inserted Code------\\
};

window.addEventListener("DOMContentLoaded", initialize);
