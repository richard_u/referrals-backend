use anyhow::{anyhow, Result};
use blake2::digest::{Update, VariableOutput};
use blake2::VarBlake2b;
use regex::Regex;
use sodiumoxide::crypto::generichash::hash;
use sodiumoxide::crypto::hash::sha256;
use sodiumoxide::crypto::sign::ed25519::{verify_detached, PublicKey, Signature};
use sodiumoxide::hex;
use std::convert::TryInto;

fn to_fixed_bytes(bytes: &[u8]) -> [u8; 64] {
    bytes.try_into().expect("Slice with incorrect length.")
}

// https://gitlab.com/tzsuite/tzutil/-/blob/master/util.go#L112
fn check_encode(input: &[u8], prefix: &[u8]) -> String {
    let mut bytes = vec![];
    bytes.append(&mut prefix.to_vec());
    bytes.append(&mut input.to_vec());
    let checksum = checksum(&bytes);
    bytes.append(&mut checksum.to_vec());
    bs58::encode(bytes).into_string()
}

// https://gitlab.com/tzsuite/tzutil/-/blob/master/util.go#L157
fn hash_address(pk: &PublicKey) -> Result<String> {
    let mut hasher = VarBlake2b::new(20)?;
    hasher.update(&pk);
    let digest = hasher.finalize_boxed();
    Ok(check_encode(&digest, &[6, 161, 159]))
}

// Tezos client request [pk, signature, msg]
pub fn verify(pk: &str, signature: &str, msg: &str) -> Result<(String, String)> {
    let edsig = [9, 245, 205, 134, 18];
    let edpk = [13, 15, 37, 217];
    let raw_signature = bs58::decode(signature).into_vec()?;
    // signature needs to be [u8; 64], so we can create Signature struct,
    // slice prefix and cut remaining bytes (edsig.len() + 64)
    // !(raw_signature.len() - edsig.len() probably works as well)!
    let sliced_prefix = &raw_signature[edsig.len()..edsig.len() + 64];
    // creates signature, we use helper to_fixed_bytes function, because new
    // method expects [u8; 64], sliced prefix is [u8]
    let signature = Signature::new(to_fixed_bytes(sliced_prefix));
    // how msg is made on client ?
    //
    // const input = ["Tezos Signed Message:", address].join(" ");
    // // https://docs.walletbeacon.io/guides/sign-payload
    // // micheline example actually printed readable text in temple
    // // 050100000042 it works with this prefix
    // const magicPrefix = "050100000042";
    // const bytes = magicPrefix + Buffer.from(input, "utf8").toString("hex");
    //
    // hex::decode on signed msg [magic_prefix, tezos_signed_string, signed_address]
    let msg = hex::decode(msg).map_err(|_e| return anyhow!("Error decoding msg."))?;
    let fingerprint = hash(&msg, Some(32), None)
        .map_err(|_e| return anyhow!("Error getting msg fingerprint."))?;
    // bs58 decode, slice edpk prefix and make PublicKey from it
    // use it in verify_detached which verifies signature againts it
    // and derive pkh in hash_address function
    let raw_pk = &bs58::decode(pk).into_vec()?;
    let sliced_pk = &raw_pk[edpk.len()..raw_pk.len() - edpk.len()];
    let pk = match PublicKey::from_slice(sliced_pk) {
        Some(pk) => pk,
        None => return Err(anyhow!("Error getting public key.")),
    };

    if verify_detached(&signature, fingerprint.as_ref(), &pk) {
        let request_pkh = hash_address(&pk)?;
        let signed_pkh = String::from_utf8(msg)?;
        // remove `Tezos signed message: ` bytes from signed msg
        // return sliced signed msg (referrer/signed pkh)
        // with pkh derived from pk (requested pkh)
        Ok(((&signed_pkh[28..]).to_string(), request_pkh))
    } else {
        Err(anyhow!("Wrong tezos signature."))
    }
}

fn map_prefix_to_bytes(prefix: &str) -> Result<[i32; 3]> {
    match prefix {
        "tz1" => Ok([6, 161, 159]),
        "tz2" => Ok([6, 161, 161]),
        "tz3" => Ok([6, 161, 164]),
        "KT1" => Ok([2, 90, 121]),
        _ => Err(anyhow!("Prefix mask did not match.")),
    }
}

// https://gitlab.com/tzsuite/tzutil/-/blob/master/util.go#L35
fn checksum(input: &[u8]) -> Vec<u8> {
    let h1 = sha256::hash(input);
    let h2 = sha256::hash(&h1[..]);
    h2[..4].to_vec()
}

pub fn is_valid_address(address: &str) -> Result<bool> {
    // capturing prefix is from taquito validateAddress method
    // maybe for our sale just accept `tz1` addresses
    // pk should be probably checked in verify function
    let implicit_prefixes = vec!["tz1", "tz2", "tz3", "KT1"];
    let re = Regex::new(&format!("^{}", implicit_prefixes.join("|"))).unwrap();
    let captured_prefix = if let Some(captured) = re.captures(address) {
        match captured.get(0) {
            Some(prefix) => prefix.as_str(),
            None => return Err(anyhow!("Error while getting captured prefix.")),
        }
    } else {
        return Err(anyhow!("No prefix matched."));
    };

    let bytes_len = map_prefix_to_bytes(captured_prefix)?.len();
    let decoded = bs58::decode(address)
        .into_vec()
        .map_err(|_e| return anyhow!("Wrong tezos address. (decode)"))?;

    if decoded.len() < 4 + bytes_len {
        return Err(anyhow!("Invalid format."));
    }

    let csum = &decoded[decoded.len() - 4..];
    let compare = checksum(&decoded[..decoded.len() - 4]);
    if &compare[..4] != csum {
        return Err(anyhow!("Invalid checksum."));
    }

    Ok(true)
}

#[cfg(test)]
mod tests {
    use crate::referral::verify::tezos::{is_valid_address, verify};

    #[test]
    fn tezos_valid() {
        // bad test, requested and signed address is the same, now impossible to make
        // same address check handled in /set_referrer endpoint
        let signature = "edsigte6wJVwMbNEbim6HTVGhTPManGUi6mgSy2Tzntvk9XdNPdHXr3kGnbQJHC6qJukZDHJSEmf2k3KEtfaDNmH3613Fh9cRDk";
        let msg = "05010000004254657a6f73205369676e6564204d6573736167653a20747a31684568625846474a546a345a73516643666a42636a74737a4c50375a483436345a";
        let pk = "edpkvUJVyRDYzpWsTJ5PDUKkXC26PrbsXd6RCsDZHWXnxyNCnk4aMH";
        // user address and referrer address
        let address = "tz1hEhbXFGJTj4ZsQfCfjBcjtszLP7ZH464Z";
        let (signed, requested) = verify(pk, signature, msg).unwrap();
        assert!(address == signed);
        assert!(address == requested);
    }

    #[test]
    fn tezos_valid_better() {
        let signature = "edsigtbKA9P9eYnGgST6QUGHJA7kbNetYNhtdTVoLcWx9uz7zT55uLL9JX2vhBZHBLttrm5s6KNwBaT2p22Mv61oMC2mVirQavu";
        let msg = "05010000004254657a6f73205369676e6564204d6573736167653a20747a31585a4875734557526f45453768414e454d6978564746576a53656d633368657968";
        let pk = "edpkuHmQ8w68n1xQAjUUder1SBNa7aQGxfeTXEaRnCkZcEQhJ6Q497";
        // connected temple wallet address
        let requested_address = "tz1hwmXHNVC5RdFu22uT7fPR638yftUn7YrD";
        // referrer address from link is signed
        let signed_address = "tz1XZHusEWRoEE7hANEMixVGFWjSemc3heyh";
        let (signed, requested) = verify(pk, signature, msg).unwrap();
        assert!(signed_address == signed);
        assert!(requested_address == requested);
    }

    #[test]
    fn tezos_invalid() {
        let signature = "edsigte6wJVwMbNEbim6HTVGhTPManGUi6mgSy2Tzntvk9XdNPdHXr3kGnbQJHC6qJukZDHJSEmf2k3KEtfaDNmH3613Fh9cRDk";
        let msg = "05010000004254657a6f73205369676e6564204d6573736167653a20747a31684568625846474a546a345a73516643666a42636a74737a4c50375a483436345a";
        let pk = "edpkuHmQ8w68n1xQAjUUder1SBNa7aQGxfeTXEaRnCkZcEQhJ6Q497";
        assert!(verify(pk, signature, msg).is_err())
    }

    #[test]
    fn address_valid() {
        assert!(is_valid_address("tz1hEhbXFGJTj4ZsQfCfjBcjtszLP7ZH464Z").is_ok());
        assert!(is_valid_address("tz1hwmXHNVC5RdFu22uT7fPR638yftUn7YrD").is_ok());
        assert!(is_valid_address("KT18gf1UzKGnd7tJXKug7EzNjZGSXtrNxUj8").is_ok());
    }

    #[test]
    fn address_invalid() {
        assert!(is_valid_address("wrongaddress").is_err());
        assert!(is_valid_address("KT18gf1$zKGnd7tJXKug7EzNjZGSXtrNxUj8").is_err());
        assert!(is_valid_address("tz1hwmXHNVC5RdFu22uT7fPR638yftYrD").is_err());
        assert!(is_valid_address("tz2hEhbXFGJTj4ZsQfCfjBcjtszLP7ZH464Z").is_err());
    }
}
