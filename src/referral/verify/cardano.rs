use anyhow::{anyhow, Result};
use cardano_serialization_lib as csl;
use emurgo_message_signing as ms;
use ms::utils::{FromBytes, ToBytes};
use ms::{COSESign1, Label};
use sodiumoxide::hex;

// Cardano client request [signature]
pub fn verify(signature: &str) -> Result<(String, String)> {
    let signature = hex::decode(signature).map_err(|_| return anyhow!("Signature decode fail."))?;
    let cosesign1 = ms::COSESign1::from_bytes(signature)
        .map_err(|error| return anyhow!("Error creating CoseSign1 struct. {}", error))?;
    let headers = cosesign1.headers();
    let tezos_address = if let Some(payload) = cosesign1.payload() {
        String::from_utf8(payload)?
    } else {
        return Err(anyhow!("Error getting payload from COSESign1."));
    };
    let pk_bytes = match headers.protected().deserialized_headers().key_id() {
        Some(b) => b,
        None => return Err(anyhow!("Error getting key from headers.")),
    };
    let pk = csl::crypto::PublicKey::from_bytes(&pk_bytes)
        .map_err(|error| return anyhow!("Error creating PublicKey struct. {}", error))?;
    let sig_struct_reconstruct = cosesign1
        .signed_data(None, None)
        .map_err(|error| return anyhow!("Error while reconstructing CoseSign1 struct. {}", error))?
        .to_bytes();
    let sig = csl::crypto::Ed25519Signature::from_bytes(cosesign1.signature())
        .map_err(|error| return anyhow!("Error creating signature. {}", error))?;

    if pk.verify(&sig_struct_reconstruct, &sig) {
        Ok((get_signature_address(cosesign1)?, tezos_address))
    } else {
        Err(anyhow!("Wrong Cardano signature."))
    }
}

// fn get_client_address(client_address: &str) -> Result<String> {
//     let decoded_client_address =
//         hex::decode(client_address).map_err(|_| return anyhow!("Address decode fail."))?;
//     let cardano_client_address = csl::address::Address::from_bytes(decoded_client_address)
//         .map_err(|_| return anyhow!("Error while getting address from bytes."))?;
//     let readable_address = cardano_client_address
//         .to_bech32(None)
//         .map_err(|_| return anyhow!("Error while getting string address."))?;

//     Ok(readable_address)
// }

fn get_signature_address(cosesign1: COSESign1) -> Result<String> {
    let address_bytes = cosesign1
        .headers()
        .protected()
        .deserialized_headers()
        .header(&Label::new_text("address".to_owned()));

    let address_bytes = if let Some(a) = address_bytes {
        a.as_bytes()
    } else {
        return Err(anyhow!("Error getting address from signature headers."));
    };

    if let Some(address) = address_bytes {
        let cardano_signature_address = csl::address::Address::from_bytes(address)
            .map_err(|_| return anyhow!("Error while getting address from bytes."))?;
        let readable_address = cardano_signature_address.to_bech32(None).map_err(|_| {
            return anyhow!("Error while getting string address.");
        })?;

        Ok(readable_address)
    } else {
        Err(anyhow!("Error getting address from CBOR value."))
    }
}

#[cfg(test)]
mod tests {
    use crate::referral::verify::cardano::verify;

    #[test]
    fn cardano_valid() {
        let signature = "845869a301270458200545073724a4940ea71c382552fc6f8334a2a8ed1349163b2077e01af7a42115676164647265737358390117cc4a7e93c63f5aea5f50a171d43253c8489fb1e0c4c10d521fa22f5532b1c2fdbc5937c4f6e59d74d1868cea538d369d08d9f45e8a35e1a166686173686564f45824747a31684568625846474a546a345a73516643666a42636a74737a4c50375a483436345a58403167d864635689e4a17a7d6298ce7a70a33d7ac2232c9fbf13a647f504fd49d49a177a004b37de6b7a7b29c561c4f117ae603b18cab9141b66904ffdbc5d5b09";
        let test_address = "addr1qytucjn7j0rr7kh2tag2zuw5xffusjylk8svfsgd2g06yt64x2cu9ldutymufah9n46drp5vaffc6d5aprvlgh52xhss2rnly9";
        let (cardano_address, _tezos_address) = verify(signature).unwrap();
        assert!((test_address == cardano_address));
    }

    #[test]
    fn cardano_invalid() {
        let signature = "845869a301270458200545073724a4940ea71c382552fc6f8334a2a8ed1349163b2077e01af7a42115676164647265737358390117cc4a7e93c63f5aea5f50a171d43253c8489fb1e0c4c10d521fa22f5532b1c2fdbc5937c4f6e59d74d1868cea538d369d08d9f45e8a35e1a166686173686564f45824747a31684568625846474a546a345a73516643666a42636a74737a4c50375a483436345a58403167d864635689e4a17a7d6298ce7a70a33d7ac2232c9fbf13a647f504fd49d49a177a004b37de6b7a7b29c561c4f117ae603b18cab9141b66904ffdbc5d5b09";
        let test_address = "addr1qytucjn7j0rr7kh2tag2zuw5xffusjylk8svfsgd2g06yt64x2cu9ldutymufah9n26drp5vaffc6d5aprvlgh52xhss2rnly9";
        let (cardano_address, _tezos_address) = verify(signature).unwrap();
        assert!(!(test_address == cardano_address));
    }
}
