use anyhow::{anyhow, Result};
use ethsign::Signature;
use sodiumoxide::hex;
use web3::types::Recovery;

// can't remember where I found this, but probably some node js lib
// https://github.com/ChainSafe/web3.js/blob/5d027191c5cb7ffbcd44083528bdab19b4e14744/docs/web3-eth-personal.rst#sign
pub fn hash_message(message: &[u8]) -> [u8; 32] {
    let prefix = format!("\u{19}Ethereum Signed Message:\n{}", message.len())
        .as_bytes()
        .to_owned();
    web3::signing::keccak256(&[prefix, message.to_vec()].concat())
}

// Ethereum client request [address, signature, msg]
pub fn verify(address: &str, signature: &str, msg: &str) -> Result<String> {
    let raw_signature =
        hex::decode(signature).map_err(|_| return anyhow!("Signature decode fail."))?;
    let msg = hex::decode(msg).map_err(|_| return anyhow!("Message decode fail."))?;
    // hash msg with ethereum signed string, so we get recovery from raw signature
    let hashed_msg = hash_message(&msg);
    // we need some things for signature struct (recovery_id, r, s)
    let recovery = Recovery::from_raw_signature(hashed_msg, &raw_signature)?;
    let recovery_id = match recovery.recovery_id() {
        Some(r) => r,
        None => return Err(anyhow!("Error accessing recovery id.")),
    };
    let signature = Signature {
        v: recovery_id as u8,
        r: *recovery.r.as_fixed_bytes(),
        s: *recovery.s.as_fixed_bytes(),
    };
    // recover signer of the msg. hashed_msg is derived from
    // msg so it should be safe to get tezos address just from
    // msg later
    let pk = signature.recover(&hashed_msg)?;
    let signed_address = hex::encode(pk.address());

    // check if signer address is the same as address we send
    if signed_address == address {
        // our hex decoded msg should be signed tezos address
        let tezos_address = String::from_utf8(msg)?;
        Ok(tezos_address)
    } else {
        Err(anyhow!("Signed address doesn't match with client address."))
    }
}

#[cfg(test)]
mod tests {
    use crate::referral::verify::ethereum::verify;

    #[test]
    fn ethereum_valid() {
        let address = "1c141f3e4b1ab349fefc1efc8db0959eeba124fe";
        let msg = "706c7320776f726b206f6d6667";
        let signature = "84e66440272b2740a7700a8d1db1a81cbb34fa54ac1715ded625f9e2e2993ada17d1dd8d811e4924b82aa8321d099a01c588604ec4d3d19b1c499e94f93326211c";
        let decoded_msg = "pls work omfg";
        assert!((decoded_msg == verify(address, signature, msg).unwrap()))
    }

    #[test]
    fn ethereum_invalid() {
        let address = "1c141f3e4b1ab349fefc1efc8db0959eeba124fe";
        let signature = "84e66440272b2740a7700a8d1db1a81cbb34fa54ac1715ded625f9e2e2993ada17d1dd8d811e4924b82aa8321d099a01c588604ec4d3d19b1c499e94f93326211c";
        let msg = "747A3168776D58484E564335526446753232755437665052363338796674556E37597244";
        assert!(verify(address, signature, msg).is_err());
    }
}
