use crate::referral::model::{
    Contributor, ContributorCardanoAddress, ContributorEVMAddress, ReferralPairing,
};
use crate::referral::verify::{cardano, ethereum, tezos};
use actix_web::{get, post, web, HttpResponse, Responder};
use serde::Deserialize;
use sqlx::SqlitePool;

#[derive(Deserialize)]
pub struct EthereumRequest {
    address: String,
    signature: String,
    msg: String,
}

#[derive(Deserialize)]
pub struct CardanoRequest {
    signature: String,
}

#[derive(Deserialize)]
pub struct ReferralRequest {
    pk: String,
    signature: String,
    msg: String,
}

#[post("/ethereum")]
pub async fn add_eth_contributor(
    pool: web::Data<SqlitePool>,
    request: web::Json<EthereumRequest>,
) -> impl Responder {
    let request = request.into_inner();
    let address = ethereum::verify(&request.address, &request.signature, &request.msg);

    match address {
        Ok(tezos_address) => match tezos::is_valid_address(&tezos_address) {
            Ok(_) => {
                let evm_contributor = ContributorEVMAddress {
                    contributor_tezos_address: tezos_address,
                    contributor_evm_address: request.address,
                };
                let db_response = ContributorEVMAddress::upsert(&pool, evm_contributor).await;
                match db_response {
                    Ok(response) => HttpResponse::Ok().json(response),
                    Err(_e) => HttpResponse::InternalServerError().finish(),
                }
            }
            Err(_e) => HttpResponse::BadRequest().body(format!("Signed address is invalid.")),
        },
        Err(_e) => HttpResponse::BadRequest().body(format!("Error while verifying eth signature.")),
    }
}

#[post("/cardano")]
pub async fn add_cardano_contributor(
    pool: web::Data<SqlitePool>,
    request: web::Json<CardanoRequest>,
) -> impl Responder {
    let request = request.into_inner();
    let addresses = cardano::verify(&request.signature);

    match addresses {
        Ok((cardano_address, tezos_address)) => match tezos::is_valid_address(&tezos_address) {
            Ok(_) => {
                let cardano_contributor = ContributorCardanoAddress {
                    contributor_tezos_address: tezos_address,
                    contributor_cardano_address: cardano_address,
                };
                let db_response =
                    ContributorCardanoAddress::upsert(&pool, cardano_contributor).await;
                match db_response {
                    Ok(response) => HttpResponse::Ok().json(response),
                    Err(_e) => HttpResponse::InternalServerError().finish(),
                }
            }
            Err(_e) => HttpResponse::BadRequest().body(format!("Signed address is invalid.")),
        },
        Err(_e) => {
            HttpResponse::BadRequest().body(format!("Error while verifying cardano signature."))
        }
    }
}

#[post("/set_referrer")]
pub async fn set_referrer(
    pool: web::Data<SqlitePool>,
    request: web::Json<ReferralRequest>,
) -> impl Responder {
    let request = request.into_inner();
    let addresses = tezos::verify(&request.pk, &request.signature, &request.msg);

    match addresses {
        Ok((signed, requested)) => match tezos::is_valid_address(&signed) {
            Ok(_) => {
                if signed == requested {
                    return HttpResponse::BadRequest().body(format!("Same addresses."));
                };
                let referral = ReferralPairing {
                    contributor_tezos_address: requested,
                    referrer_tezos_address: signed,
                };
                let db_response = ReferralPairing::overwrite_referrer(&pool, referral).await;
                match db_response {
                    Ok(response) => HttpResponse::Ok().json(response),
                    Err(_e) => HttpResponse::InternalServerError().finish(),
                }
            }
            Err(_e) => HttpResponse::BadRequest().body(format!("Signed address is invalid.")),
        },
        Err(_e) => {
            HttpResponse::BadRequest().body(format!("Error while verifying tezos signature."))
        }
    }
}

// get_referrer is kinda useless just get the contributor
// #[get("/get_referrer/{address}")]
// pub async fn get_referrer(
//     pool: web::Data<SqlitePool>,
//     web::Path(address): web::Path<String>,
// ) -> impl Responder {
//     let db_response = ReferralPairing::get_referrer(&pool, &address).await;
//     match db_response {
//         Ok(response) => HttpResponse::Ok().json(response),
//         Err(_e) => HttpResponse::InternalServerError().finish(),
//     }
// }

#[get("/contributor/{address}")]
pub async fn get_contributor(
    pool: web::Data<SqlitePool>,
    web::Path(address): web::Path<String>,
) -> impl Responder {
    let contributor = Contributor::get_by_address(&pool, &address).await;

    match contributor {
        Ok(c) => HttpResponse::Ok().json(c),
        Err(_e) => HttpResponse::BadRequest().body(format!("Error while getting contributor.")),
    }
}
