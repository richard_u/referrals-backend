use anyhow::Result;
use serde::Serialize;
use sqlx::{FromRow, SqlitePool};

#[derive(FromRow, Serialize)]
pub struct ReferralPairing {
    pub contributor_tezos_address: String,
    pub referrer_tezos_address: String,
}

#[derive(FromRow, Serialize)]
pub struct ContributorEVMAddress {
    pub contributor_tezos_address: String,
    pub contributor_evm_address: String,
}

#[derive(FromRow, Serialize)]
pub struct ContributorCardanoAddress {
    pub contributor_tezos_address: String,
    pub contributor_cardano_address: String,
}

#[derive(Serialize)]
pub struct Contributor {
    pub contributor_tezos_address: String,
    pub referrer_tezos_address: String,
    pub contributor_cardano_addresses: Vec<String>,
    pub contributor_evm_addresses: Vec<String>,
}

impl ReferralPairing {
    pub async fn overwrite_referrer(pool: &SqlitePool, pair: Self) -> Result<Self> {
        let mut conn = pool.acquire().await?;

        sqlx::query!(
            r#"INSERT OR REPLACE INTO contributors(contributor_tezos_address, referrer_tezos_address) VALUES ($1, $2)"#,
            pair.contributor_tezos_address,
            pair.referrer_tezos_address
        )
        .execute(&mut conn)
        .await?;

        Ok(pair)
    }

    // pub async fn get_referrer(pool: &SqlitePool, address: &str) -> Result<Self> {
    //     let pairing = sqlx::query_as!(
    //         ReferralPairing,
    //         r#"SELECT contributor_tezos_address, referrer_tezos_address FROM contributors WHERE contributor_tezos_address = $1"#,
    //         address
    //     )
    //     .fetch_one(&*pool)
    //     .await?;

    //     Ok(pairing)
    // }
}

impl ContributorEVMAddress {
    pub async fn upsert(pool: &SqlitePool, e: Self) -> Result<Self> {
        let mut tx = pool.begin().await?;

        sqlx::query!(
            "INSERT INTO contributors(contributor_tezos_address, referrer_tezos_address)
            VALUES ($1, $2)
            ON CONFLICT (contributor_tezos_address) DO
            UPDATE SET contributor_tezos_address = contributor_tezos_address",
            e.contributor_tezos_address,
            "",
        )
        .execute(&mut tx)
        .await?;

        sqlx::query!(
            "INSERT INTO evm_addresses(contributor_tezos_address, contributor_evm_address)
             VALUES ($1, $2)",
            e.contributor_tezos_address,
            e.contributor_evm_address,
        )
        .execute(&mut tx)
        .await?;

        tx.commit().await?;

        Ok(Self {
            contributor_tezos_address: e.contributor_tezos_address,
            contributor_evm_address: e.contributor_evm_address,
        })
    }
}

impl ContributorCardanoAddress {
    pub async fn upsert(pool: &SqlitePool, c: Self) -> Result<Self> {
        let mut tx = pool.begin().await?;

        sqlx::query!(
            "INSERT INTO contributors(contributor_tezos_address, referrer_tezos_address)
            VALUES ($1, $2)
            ON CONFLICT (contributor_tezos_address) DO
            UPDATE SET contributor_tezos_address = contributor_tezos_address",
            c.contributor_tezos_address,
            ""
        )
        .execute(&mut tx)
        .await?;

        sqlx::query!(
            "INSERT INTO cardano_addresses(contributor_tezos_address, contributor_cardano_address)
             VALUES ($1, $2)",
            c.contributor_tezos_address,
            c.contributor_cardano_address,
        )
        .execute(&mut tx)
        .await?;

        tx.commit().await?;

        Ok(Self {
            contributor_tezos_address: c.contributor_tezos_address,
            contributor_cardano_address: c.contributor_cardano_address,
        })
    }
}

impl Contributor {
    pub async fn get_by_address(pool: &SqlitePool, address: &str) -> Result<Self> {
        let mut tx = pool.begin().await?;

        let contributor = sqlx::query_as!(
            ReferralPairing,
            "SELECT contributor_tezos_address, referrer_tezos_address FROM contributors WHERE contributor_tezos_address = $1",
            address
        )
        .fetch_one(&mut tx)
        .await?;

        let contributor_cardano_addresses = sqlx::query!(
            "SELECT contributor_cardano_address FROM cardano_addresses WHERE contributor_tezos_address = $1",
            address
        )
        .fetch_all(&mut tx)
        .await?;

        let mut cardano_addresses = Vec::new();
        for cardano in contributor_cardano_addresses {
            cardano_addresses.push(cardano.contributor_cardano_address)
        }

        let contributor_evm_addresses = sqlx::query!(
            "SELECT contributor_evm_address FROM evm_addresses WHERE contributor_tezos_address = $1",
            address
        )
        .fetch_all(&mut tx)
        .await?;

        let mut evm_addresses = Vec::new();
        for evm in contributor_evm_addresses {
            evm_addresses.push(evm.contributor_evm_address)
        }

        tx.commit().await?;

        Ok(Contributor {
            contributor_tezos_address: contributor.contributor_tezos_address,
            referrer_tezos_address: contributor.referrer_tezos_address,
            contributor_cardano_addresses: cardano_addresses,
            contributor_evm_addresses: evm_addresses,
        })
    }
}
