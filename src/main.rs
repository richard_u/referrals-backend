use actix_cors::Cors;
use actix_web::{get, middleware::Logger, App, HttpResponse, HttpServer, Responder};
use anyhow::Result;
use dotenv::dotenv;
use sqlx::SqlitePool;
use std::env;

mod referral;

#[get("/hc")]
async fn health_check() -> impl Responder {
    HttpResponse::Ok()
}

#[actix_web::main]
async fn main() -> Result<()> {
    dotenv().ok();
    env_logger::init();

    let db_url = env::var("DATABASE_URL").expect("DATABASE_URL is not set in .env file");
    let db_pool = SqlitePool::connect(&db_url).await?;
    // cors setup for prod, fix url if needed, maybe add .allowed_headers()
    let _cors = Cors::default()
        .allowed_origin_fn(|origin, _| origin.as_bytes().ends_with(b".sexp.exchange"))
        .allowed_methods(vec!["GET", "POST"])
        .max_age(3600);

    HttpServer::new(move || {
        App::new()
            .data(db_pool.clone())
            .wrap(Cors::permissive())
            .wrap(Logger::default())
            .service(health_check)
            .service(referral::add_eth_contributor)
            .service(referral::add_cardano_contributor)
            .service(referral::set_referrer)
            .service(referral::get_contributor)
        //.service(referral::get_referrer)
    })
    .bind("127.0.0.1:8080")?
    .run()
    .await?;

    Ok(())
}
