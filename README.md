## Project setup

Install sqlx-cli so we can run migrations

`cargo install sqlx-cli`

`sqlx database prepare`

`sqlx migrate run`

`cargo run`
