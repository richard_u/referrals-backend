#!/bin/bash

DB="./db/referral.db"
BKUP_PATH="./db/backup"

# seconds since epoch
DATE="$(date +'%s')"

# open database, wait up to 10 seconds for any activity to end and create a backup file
sqlite3 ${DB} << EOF
.timeout 10000
.backup ${BKUP_PATH}/referraldb_${DATE}.sqlite3
EOF